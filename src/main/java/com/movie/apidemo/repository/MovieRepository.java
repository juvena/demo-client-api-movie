package com.movie.apidemo.repository;

import java.util.List;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.movie.apidemo.model.MovieModel;

@RepositoryRestResource(collectionResourceRel = "movie", path = "movie")
public interface MovieRepository extends MongoRepository<MovieModel, String>{
	List<MovieModel> findByTitle(@Param("title") String title);
	List<MovieModel> findByReleaseDate(@Param("releaseDate") String releaseDate);
	List<MovieModel> findByOverview(@Param("overview") String overview);
	
}
