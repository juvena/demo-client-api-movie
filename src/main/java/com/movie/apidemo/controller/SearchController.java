package com.movie.apidemo.controller;

import org.springframework.stereotype.Controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.movie.apidemo.config.MongoConfig;
import com.movie.apidemo.model.MovieModel;
import com.movie.apidemo.model.QueryModel;
import com.movie.apidemo.repository.MovieRepository;

@Controller
public class SearchController {
	@Autowired MovieRepository movieRepository;

	final String uri = "https://api.themoviedb.org/3/search/movie?api_key=07861c739e64fc33902f832a1fa55c83&language=en-US";
	ApplicationContext ctx = new AnnotationConfigApplicationContext(MongoConfig.class);
	MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

	/**
	 * search movie. At first, search in mongoDB, if does not exist, try to find in external API
	 * **/
	@RequestMapping(value = "/movie")
	public Object searchAMovie(
			@RequestParam(value="title", defaultValue="") String title, 
			@RequestParam(value="genre", defaultValue="") String genre, 
			@RequestParam(value="from", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate,
			@RequestParam(value="to", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate) {

		ResponseEntity<List<MovieModel>> response = null; 

		if (title.equals("") && genre.equals("") && fromDate==null && toDate==null) {
			response = new ResponseEntity<List<MovieModel>>(new ArrayList<MovieModel>(), HttpStatus.NOT_ACCEPTABLE);
		}else {
			QueryModel qmodel = new QueryModel(title, genre, fromDate.toString(), toDate.toString());
			System.out.println("AAAAAAAAAAAAAAAAAAA");
			System.out.println(qmodel.generateQuery());
			System.out.println("AAAAAAAAAAAAAAAAAAA");
			//trying to find in MondoDB()
			response = getMongoData(qmodel);

			if (response.getStatusCodeValue() == 404){
				//trying to find in external API()
				getExternalData(title);
				response = getMongoData(qmodel);
			}
		}
		System.out.println(response);
		return response;
	}


	public void getExternalData(String query) {
		List<MovieModel> moviesResult = new ArrayList<MovieModel>();

		HttpHeaders headers = new HttpHeaders();
		RestTemplate restTemplate = new RestTemplate();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		List<MovieModel> response2 = findByQuery("{\"title\": /.*" + query + ".*/i}");

		if (response2.isEmpty()){

			int sizeResult = -1;
			int pageIterator = 0;
			do {
				pageIterator++;
				ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(uri+"&query="+query+"&page="+pageIterator, HttpMethod.GET, entity, new ParameterizedTypeReference<Map<String, Object>>() {});
				ArrayList<Map<String, Object>> response = (ArrayList<Map<String, Object>>) responseEntity.getBody().get("results");
				sizeResult = response.size();
				result.addAll(response);
				System.out.println("quantidade de resultados na pagina ="+ pageIterator + "=: "+sizeResult);
			}while(sizeResult != 0);
			if (!moviesResult.isEmpty()){moviesResult = insertMongoData(result);}

		}	    
	}

	public ResponseEntity<List<MovieModel>> getMongoData(QueryModel qModel){
		
		List<MovieModel> response = findByQuery(qModel.generateQuery());

		//verifying if already push all similar titles
		List<MovieModel> response2 = findByQuery("{\"title\": /.*"+ qModel.getTitle() +".*/i}");

		ResponseEntity<List<MovieModel>> result;
		if (response.isEmpty() && response2.isEmpty()) {
			result = new ResponseEntity<List<MovieModel>>(response, HttpStatus.NOT_FOUND);
		}else {
			result = new ResponseEntity<List<MovieModel>>(response, HttpStatus.OK);
		}
		return result;
	}

	public List<MovieModel> insertMongoData(ArrayList<Map<String, Object>> results){
		List<MovieModel> responseMongo = new ArrayList<MovieModel>();
		for(Map<String, Object> result : results) {

			MovieModel resultModel = new MovieModel(result.get("title").toString(), 
					result.get("overview").toString(),result.get("release_date").toString());

			System.out.println("inserindo title: " + resultModel.getTitle() + " -> release_date: " + resultModel.getReleaseDate());
			responseMongo.add(resultModel);
			movieRepository.insert(resultModel);

		}
		return responseMongo;
	}

	public List<MovieModel> findByQuery(String query) {
		//		BasicQuery query1 = new BasicQuery("{\"title\": /.*Ouija Board.*/i}");
		BasicQuery bQuery = new BasicQuery(query);
		List<MovieModel> response = mongoOperation.find(bQuery, MovieModel.class);

		return response;
	}

}
