package com.movie.apidemo.model;

public class QueryModel {

	private String title;
	private String genre;
	private String startReleaseDate;
	private String endReleaseDate;

	public QueryModel(String title, String genre, String startReleaseDate, String endReleaseDate) {
		this.title = title;
		this.genre = genre;
		this.startReleaseDate = startReleaseDate;
		this.endReleaseDate = endReleaseDate;
	}

	public boolean isValidReleasePeriod() {
		boolean isValid = false;
		if (this.startReleaseDate != null && this.endReleaseDate != null){
			isValid = true;
		}
		return isValid;
	}

	public int countValidFields() {
		int count = 0;
		if (title != null  && !title.equals("")) {
			count++;
		}if (genre != null && !genre.equals("")) {
			count++;
		}if (isValidReleasePeriod()) {
			count++;
		}

		return count;

	}

	public String generateQuery(){
		int validFields = countValidFields();
		String query = "";
		
		boolean first = true;
		if (isValidReleasePeriod()) {
			query="{releaseDate: {$gte: \""+ this.startReleaseDate +"\" ,$lte: \"" + this.endReleaseDate+"\"}}";
			if (first){first = false;}
		}if (this.title != null && !title.equals("")) {
			if (first){first = false;}else {query+=",";}
			query += "{title: {$eq: \"" + this.title + "\"}}";
		}if (genre != null && !genre.equals("")) {
			if (first){first = false;}else {query+=",";}
			query += "{genre: {$eq: \"" + this.genre + "\"}}";
		}
		
			//    		"{ $and: [ { price: { $ne: 1.99 } }, { price: { $exists: true } } ] }"
		if (validFields > 1) {
			query = "{ $and: [ " + query + "] }";
		}

		return query;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getStartReleaseDate() {
		return startReleaseDate;
	}

	public void setStartReleaseDate(String startReleaseDate) {
		this.startReleaseDate = startReleaseDate;
	}

	public String getEndReleaseDate() {
		return endReleaseDate;
	}

	public void setEndReleaseDate(String endReleaseDate) {
		this.endReleaseDate = endReleaseDate;
	}
}
