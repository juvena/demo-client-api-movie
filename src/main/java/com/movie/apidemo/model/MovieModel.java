package com.movie.apidemo.model;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "movies")
public class MovieModel {
	
	@Indexed
	private String title;
	@Indexed
    private String genre;
	@Indexed
    private String overview;
	@Indexed
	private String releaseDate;
	
	public MovieModel(){}
    public MovieModel(String title, String overview, String releaseDate){
    	this.releaseDate = releaseDate;
    	this.title = title;
    	this.overview = overview;
    }

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSynopsis() {
		return overview;
	}
	public void setSynopsis(String overview) {
		this.overview = overview;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
    
}

