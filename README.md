### Description ###
This Project loads Movies into MongoDB and search them.

### Usage ###

Implemented endpoints:

/movie?from=yyyy-MM-dd&to=yyyy-MM-dd&title={title}
/movie?from=yyyy-MM-dd&to=yyyy-MM-dd
/movie?title={title}

### Next Steps ###
ElasticSearch Index

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

